#setdep @node|-1@

File {
    Grid       = "n@node|-1@_msh.tdr"
    Current    = "@plot@"
    Plot       = "@tdrdat@"
    output = "@log@"
    parameter = "@parameter@"
}



Electrode {
	!(
	for {set i 1} {$i<=@NumStripes@} {incr i} {
					puts "\{name = \"cathode${i}\"   voltage = 0.0   eRecVelocity=1e7 hRecVelocity=1e7 \}"	
	}
	)!

	###{name = "anode"   voltage = 0.0    voltage=((-1000 0)(-100 @<-HV>@) (0 @<-HV>@))	eRecVelocity=1e7 hRecVelocity=1e7 }					
	{name = "anode"   voltage = 0.0    	eRecVelocity=1e7 hRecVelocity=1e7 }					
}


Physics { 



  	HeavyIon("ion1") (
  		Direction=(0,1)
  		Location=(@posX@,0.0)
  		Time=5e-9
  		Length = 100.0
  		LET_f = 0.3
  		Wt_hi = 0.5
  		Gaussian
  		PicoCoulomb
  		)

} 






Math {


    Derivatives
  Avalderivatives
  Notdamped=1000
  Iterations=100
  RelerrControl
  RhsMin=1e-15
  
  
  RecBoxIntegr

  Transient=BE
  
  
    Method=Blocked
  SubMethod=ILS (set=1)
  ACMethod=Blocked 
  ACSubMethod=ILS (set=2)
 
  
  ILSrc = "
           set(1) { 
                iterative( gmres(100), tolrel=1e-8, tolunprec=1e-4, tolabs=0, maxit=200 ); 
                preconditioning( ilut(0.0001,-1), left ); 
                ordering( symmetric=nd, nonsymmetric=mpsilst ); 
                options( compact=yes, verbose=0, refineresidual=0 ); 
           };
           
           set(2) { 
                iterative( gmres(100), tolrel=1e-8, tolunprec=1e-4, tolabs=0, maxit=200 ); 
                preconditioning( ilut(1e-8,-1), left ); 
                ordering( symmetric=nd, nonsymmetric=mpsilst ); 
                options( compact=yes, verbose=0, refineresidual=0 ); 
           };

           
     "

  number_of_threads=4

  Extrapolate
}


Solve {

poisson

}
   

Plot {
	
  
 	HeavyIonGeneration
 	HeavyIonChargeDensity


}


